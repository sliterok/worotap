const transformTools = require('browserify-transform-tools')
const path = require('path')
const msgpack = require('msgpack5')()
const BufferList = require('bl')
const promisify = require('util').promisify;
const fs = require('fs')
fs.readFileP = promisify(fs.readFile)

const transform = transformTools.makeRequireTransform('jsonToMp', {
	evaluateArguments: true
}, async function(args, opts, cb) {
	let filePath = args[0]
	if(!filePath.endsWith('json'))
		return cb()

	let rawFile = await fs.readFileP(filePath)

	let file = await fs.readFileP(filePath, 'utf8')
	let encoded = msgpack.encode(JSON.parse(file))
	let decoded = JSON.stringify(msgpack.decode(Buffer.from(encoded)))
	const bl = new BufferList()
	bl.append(decoded)
	bl.pipe(fs.createWriteStream(filePath.replace('.json', '.mp')))

	console.log(encoded.length / rawFile.length)
	console.log(Buffer.from(JSON.stringify(decoded)).length / rawFile.length)



	//return cb(null, `msgpack.decode('${encoded}')`);
});

module.exports = transform

let dummyJsFile = path.resolve(__dirname, "../tests/dummy.js");
let content = `require('front/mapSource.json')`;
transformTools.runTransform(transform, dummyJsFile, {
		content: content
	},
	function(err, transformed) {
		//eval(transformed)
		console.log(transformed)
		// Verify transformed is what we expect...
	}
);