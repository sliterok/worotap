const fs = require('fs')
const browserify = require('browserify');
const express = require('express')
const socketIo = require('socket.io')
const app = express()

const mongoose = require('./mongo.js')

const babelify = require('babelify')
//const sassify = require('sassify')
const lessify = require('lessify')
const bcss = require('browserify-css')
const vueify = require('vueify')

const process = require('process');
const dev = process.argv[2] === 'dev'

const disc = require('disc')

let b = browserify({
		fullPaths: false
	})
	.add('./front/index.js')
	.transform(bcss, {
		global: true
	})
	.transform(lessify)
	.transform(babelify)
	.transform(vueify)
if(!dev)
	b = b.plugin('tinyify')

b.bundle()
	.on('end', init)
	.pipe(fs.createWriteStream(__dirname + '/public/bundle.js'))

/*
browserify({
		fullPaths: true
	})
	.add('./front/index.js')
	.transform(require('browserify-css'))
	.transform(babelify)
	.transform('unassertify', {
		global: true
	})
	.transform('envify', {
		global: true
	})
	.transform('uglifyify', {
		global: true
	})
	.plugin('common-shakeify')
	.bundle()
	.pipe(require('minify-stream')({
		sourceMap: false
	}))
	.pipe(disc())
	.pipe(fs.createWriteStream(__dirname + '/public/bundle2.html'))*/


app.use(express.static('public'));

async function init() {
	await mongoose.connect()
	let socketHandlers = require('./sockets/index.js')
	let httpHandlers = require('./http/index.js')

	httpHandlers(app)

	let server = null

	if(dev)
		server = require('http').createServer(app).listen(8080)
	else
		server = require("greenlock-express")
		.create({
			email: "adm@worldroulette.ru",
			agreeTos: true,
			configDir: "~/.config/acme/",
			app: app
		}).listen(80, 443)

	const io = socketIo(server)

	io.mergeRooms = function(room1, room2) {
		const connectedSockets = io.in(room1).connected
		for(const socketId in connectedSockets) {
			const userSocket = connectedSockets[socketId]
			userSocket.join(room2)
		}
	}

	io.intersectRooms = function(room1, room2) {
		const connectedSockets = io.in(room1).connected
		for(const socketId in connectedSockets) {
			const userSocket = connectedSockets[socketId]
			userSocket.leave(room2)
		}
	}

	io.deleteRoom = function(room) {
		const connectedSockets = io.in(room).connected
		for(const socketId in connectedSockets) {
			const userSocket = connectedSockets[socketId]
			userSocket.leave(room)
		}
	}

	io.on('connection', async socket => {
		let handlers = await socketHandlers(io, socket)
		console.log('io connection')
	});

	if(dev)
		console.log('listening at :8080')
	else
		console.log('listening at :443 and :80')
}

//init()