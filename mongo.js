const MongoURL = 'mongodb://localhost:27017/worotap';
const mongoose = require('mongoose');
const models = {};

function createModel(modelName) {
	let model = null;
	let forbiddenFields = null;
	let filters = null
	switch (modelName) {
		case 'BaseItem':
			schema = new mongoose.Schema({
				id: Number,
				name: String,
				drops: {
					type: Boolean,
					default: false
				},
				stats: {
					type: Object,
					default: null
				},
				maxUses: {
					type: Number,
					default: 1
				},
				rarity: {
					type: Number,
					default: 1
				},
				cost: Number,
				price: {
					type: Number,
					default: 0
				},
				description: {
					type: String,
					default: 'Нет описания'
				}
			});

			forbiddenFields = ['_id', '__v']
			filters = '-' + forbiddenFields.join(' -')

			schema.methods.filterFields = function() {
				let filteredDocument = {}
				for(let key in this.toObject()) {
					if(!forbiddenFields.includes(key))
						filteredDocument[key] = this[key]
				}
				return filteredDocument
			}

			model = mongoose.model('BaseItem', schema);

			model.findFiltered = async function(baseItemIds) {
				return await this.find({
					id: baseItemIds
				}, filters)
			}
			model.findFilteredQuery = async function(query) {
				return await this.find(query, filters)
			}
			break;

		case 'Item':
			schema = new mongoose.Schema({
				id: Number,
				baseItem: Number,
				owner: Number,
				uses: Number,
				enabled: {
					type: Boolean,
					default: false
				},
				timeGot: {
					type: Date,
					default: Date.now
				},
				deleted: {
					type: Boolean,
					default: false
				}
			});

			forbiddenFields = ['_id', '__v']
			filters = '-' + forbiddenFields.join(' -')

			schema.methods.filterFields = function() {
				let filteredDocument = {}
				for(let key in this.toObject()) {
					if(!forbiddenFields.includes(key))
						filteredDocument[key] = this[key]
				}
				return filteredDocument
			}

			schema.pre('save', async function(next) {
				if(!this.isNew)
					return next()

				let lastItem = await models.Item.findOne().sort('-id')

				this.id = lastItem ? lastItem.id + 1 : 1
				next();
			});

			model = mongoose.model('Item', schema);

			model.findFiltered = async function(itemIds) {
				return await this.find({
					id: itemIds
				}, filters)
			}

			model.findFilteredQuery = async function(query) {
				return await this.find(query, filters)
			}
			break;

		case 'Clan':
			schema = new mongoose.Schema({
				id: Number,
				name: String,
				owner: Number,
				color: String,
				requests: {
					type: [Number],
					default: []
				}
			});

			schema.index({
				name: 'text'
			});

			forbiddenFields = ['_id', '__v']
			filters = '-' + forbiddenFields.join(' -')

			schema.methods.filterFields = function() {
				let filteredDocument = {}
				for(let key in this.toObject()) {
					if(!forbiddenFields.includes(key))
						filteredDocument[key] = this[key]
				}
				return filteredDocument
			}

			schema.pre('save', async function(next) {
				if(!this.isNew)
					return next()

				let lastClan = await models.Clan.findOne().sort('-id')

				this.id = lastClan ? lastClan.id + 1 : 1
				next();
			});

			model = mongoose.model('Clan', schema);

			model.findFiltered = async function(clanIds) {
				return await this.find({
					id: clanIds
				}, filters)
			}
			break;

		case 'Land':
			schema = new mongoose.Schema({
				code: String,
				owner: Number,
				neighbors: [String],
				area: Number,
				power: Number
			});

			forbiddenFields = ['_id', '__v']
			filters = '-' + forbiddenFields.join(' -')

			schema.methods.filterFields = function() {
				let filteredDocument = {}
				for(let key in this.toObject()) {
					if(!forbiddenFields.includes(key))
						filteredDocument[key] = this[key]
				}
				return filteredDocument
			}

			model = mongoose.model('Land', schema);
			break;

		case 'User':
			schema = new mongoose.Schema({
				id: Number,
				login: String,
				name: String,
				energy: {
					type: Number,
					default: 10
				},
				clan: {
					type: Number,
					default: null
				},
				auths: [{
					session: String,
					key: String
				}],
				password: String,
				color: {
					type: String,
					default: '#333333'
				},
				emoji: {
					type: String,
					default: ''
				},
				emojis: {
					type: Number,
					default: 0
				},
				imgur: {
					type: String,
					default: null
				},
				balance: {
					type: Number,
					default: 0
				},
				totalDonations: {
					type: Number,
					default: 0
				},
				online: {
					type: Number,
					default: 0
				},
				registered: {
					type: Date,
					default: Date.now
				},
				messages: {
					type: Number,
					default: 0
				},
				mined: {
					type: Number,
					default: 0
				},
				rollsFromLast: {
					double: {
						type: Number,
						default: 0
					},
					triple: {
						type: Number,
						default: 0
					},
					quadriple: {
						type: Number,
						default: 0
					},
					drop: {
						type: Number,
						default: 0
					}
				},
				totalsp: {
					type: Number,
					default: 0
				},
				oldPass: {
					type: String,
					default: null
				},
				email: {
					type: String,
					default: null
				}
			});

			forbiddenFields = ['auths', 'password', '_id', '__v', 'email', 'oldPass', 'registered', 'rollsFromLast', 'login']
			filters = '-' + forbiddenFields.join(' -')

			schema.methods.filterFields = function() {
				let filteredDocument = {}
				for(let key in this.toObject()) {
					if(!forbiddenFields.includes(key))
						filteredDocument[key] = this[key]
				}
				return filteredDocument
			}

			schema.pre('save', async function(next) {
				if(!this.isNew)
					return next()

				this.name = this.login

				this.id = (await models.User.findOne().sort('-id')).id + 1
				next();
			});

			model = mongoose.model('User', schema);
			model.findFiltered = async function(userIds) {
				return await this.find({
					id: userIds
				}, filters)
			}
			model.findFilteredQuery = async function(query) {
				return await this.find(query, filters)
			}
			break;
		case 'Message':
			schema = new mongoose.Schema({
				id: Number,
				from: Number,
				date: {
					type: Date,
					default: Date.now
				},
				to: {
					type: Number,
					default: null
				},
				private: {
					type: Boolean,
					default: false
				},
				users: [Number],
				items: [Number],
				text: String
			});

			forbiddenFields = ['_id', '__v', 'users', 'items']
			filters = '-' + forbiddenFields.join(' -')

			schema.methods.filterFields = function() {
				let filteredDocument = {}
				for(let key in this.toObject()) {
					if(!forbiddenFields.includes(key))
						filteredDocument[key] = this[key]
				}
				return filteredDocument
			}

			schema.pre('save', async function(next) {
				if(!this.isNew)
					return next()

				let lastMessage = await models.Message.findOne().sort('-id')

				this.id = lastMessage ? lastMessage.id + 1 : 1
				next();
			});

			model = mongoose.model('Message', schema);
			break;
	}
	models[modelName] = model;
}
createModel('Land');
createModel('Clan');
createModel('User');
createModel('Message');
createModel('BaseItem');
createModel('Item');

module.exports = {
	connect: async function() {
		await mongoose.connect(MongoURL, {
			poolSize: 100,
			connectTimeoutMS: 30000,
			socketTimeoutMS: 30000,
			keepAlive: true,
			keepAliveInitialDelay: 300000,
			autoReconnect: true,
			reconnectTries: 1000000,
			reconnectInterval: 3000
		});
		return this
	},
	mongoose: mongoose,
	models: models
};

mongoose.Promise = Promise;

mongoose.connection.on('connected', () => {
	console.log('Connection Established');
});

mongoose.connection.on('reconnected', () => {
	console.log('Connection Reestablished');
});

mongoose.connection.on('disconnected', () => {
	console.log('Connection Disconnected');
});

mongoose.connection.on('close', () => {
	console.log('Connection Closed');
});

mongoose.connection.on('error', (error) => {
	console.log('ERROR: ' + error);
});