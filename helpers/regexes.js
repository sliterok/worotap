const regexes = {
	login: /^\w{3,15}$/,
	name: /^[\wа-я ]{3,35}$/i,
	password: /^.{3,64}$/,
	color: /^#[a-f0-9]{6}$/i,
	mention: /@([0-9]{1,6})/g,
	mentionGroup: /(@[0-9]{1,6})/,
	itemMention: /\$([0-9]{1,6})/g,
	itemMentionGroup: /(\$[0-9]{1,6})/,
	clanMention: /\#([0-9]{1,6})/g,
	clanMentionGroup: /(\#[0-9]{1,6})/,
	landMention: /\!([a-z\-]{2,5})/gi,
	landMentionGroup: /(\![a-z\-]{2,5})/i,
	privateMessage: /^\/msg ([0-9]{1,5}) (.+)$/i
}

module.exports = regexes