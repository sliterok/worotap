const mongoose = require('../mongo.js')
let initialised = false
let initPromise = null

module.exports = {
	init: async function(key) {
		if(this.initialised)
			return true
		else if(!this.initialised && this.initPromise)
			return initPromise

		this.initPromise = new Promise(async resolve => {
			let object = await mongoose.models[key].find({})
			this.initialised = true
			resolve(object)
		})

		return this.initPromise
	},
	initialised: false,
	initPromise: null
}