const fs = require('fs');
const readline = require('readline');
const {
	google
} = require('googleapis');
let auth = null;

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

const secrets = require('../secrets.js')
authorize(secrets.spreadSheetCreditinals)

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
	const {
		client_secret,
		client_id,
		redirect_uris
	} = credentials.installed;
	const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

	// Check if we have previously stored a token.
	fs.readFile(TOKEN_PATH, (err, token) => {
		if(err)
			return getNewToken(oAuth2Client, callback);
		oAuth2Client.setCredentials(JSON.parse(token));
		auth = oAuth2Client;
	});
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
	const authUrl = oAuth2Client.generateAuthUrl({
		access_type: 'offline',
		scope: SCOPES,
	});

	console.log('Authorize this app by visiting this url:', authUrl);

	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
	});
	rl.question('Enter the code from that page here: ', (code) => {
		rl.close();
		oAuth2Client.getToken(code, (err, token) => {
			if(err)
				return console.error('Error while trying to retrieve access token', err);
			oAuth2Client.setCredentials(token);

			// Store the token to disk for later program executions
			fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
				if(err)
					console.error(err);
				console.log('Token stored to', TOKEN_PATH);
			});
			callback(oAuth2Client);
		});
	});
}

async function downloadTable() {
	if(!auth)
		throw new Error('Spreadsheet is not yet authorized')

	const sheets = google.sheets({
		version: 'v4',
		auth
	});

	const res = await sheets.spreadsheets.values.get({
		spreadsheetId: '19GaMuQ-GPP1W3nJW6KBO0te637wWE0kJ4BKsUF8YiE8',
		range: 'items',
	});

	if(res.err)
		throw new Error('The spreadsheet API returned an error: ' + err);

	return res.data;
}

async function getTable() {
	const table = await downloadTable();

	const rows = table.values;

	const keys = rows[0];

	const objects = rows.map(el => {
		const obj = {};
		keys.forEach((key, i) => obj[key] = el[i] === undefined ? '' : el[i]);
		return obj;
	})

	objects.shift()

	return objects
}

async function updateItem(id, item) {
	if(!auth)
		throw new Error('Spreadsheet is not yet authorized')

	const sheets = google.sheets({
		version: 'v4',
		auth
	});

	const table = await downloadTable();
	const index = table.values.findIndex(el => el[0] == id)

	const rs = await sheets.spreadsheets.values.update({
		spreadsheetId: '19GaMuQ-GPP1W3nJW6KBO0te637wWE0kJ4BKsUF8YiE8',
		valueInputOption: 'USER_ENTERED',
		range: `items!A${index+1}:K${index+1}`,
		resource: {
			values: [item]
		}
	});

	//console.log(await rs.res)
	return rs;
}

module.exports = {
	getTable: getTable,
	updateItem: updateItem
}