const funcs = {
	countItemPrice: function(rarity, price, uses, maxUses, buy) {
		if(price === 0)
			price = (rarity + 2)

		if(!buy)
			price = Math.floor(price * 0.8)

		let coeff = uses / maxUses
		if(coeff < 1)
			price = Math.floor(price * coeff)

		return price
	},
	countItemsStat: function(baseItems, key) {
		let val = 0;

		for(let baseItem of baseItems) {
			if(baseItem.stats && baseItem.stats.hasOwnProperty(key))
				val += baseItem.stats[key]
		}

		if(val > 1.5)
			val = 1.5;
		else if(val < -1.5)
			val = -1.5;

		return val;
	}
}

module.exports = funcs