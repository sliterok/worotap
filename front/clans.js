import Verte from 'verte';
import 'verte/dist/verte.css';

module.exports = (Vue, socket, storage) => {
	socket.on('foundClans', clans => storage.foundClans = clans)

	let menuEl = new Vue({
		el: '.clanMenu',
		data: storage,
		components: {
			Verte
		},
		watch: {
			searchClan: query => {
				socket.emit('searchClan', query)
			}
		},
		computed: {
			isEditNameValid: function() {
				return this.regexes.name.test(this.editClan.name)
			},
			isEditColorValid: function() {
				return this.regexes.color.test(this.editClan.color)
			},
			newClanContrastingColor: function() {
				return (this.newClan.color.replace('#', '0x')) > (0xffffff / 2) ? '#000' : '#fff';
			},
			editClanContrastingColor: function() {
				return (this.editClan.color.replace('#', '0x')) > (0xffffff / 2) ? '#000' : '#fff';
			},
			newClanValid: function() {
				return this.regexes.name.test(this.newClan.name) && this.regexes.color.test(this.newClan.color)
			},
			editClanValid: function() {
				return this.regexes.name.test(this.editClan.name) && this.regexes.color.test(this.editClan.color)
			}
		},
		methods: {
			shareClan: function() {
				socket.emit('chatWrite', `#${this.users[this.me].clan}`)
			},
			saveValue: function(key) {
				let val = this.editClan[key]
				let valid = this.regexes[key] ? this.regexes[key].test(val) : true
				if(!valid)
					return console.error(`tried to save ${key} but it's value of "${val}" is invalidated on client-side`)

				socket.emit('editClan', key, val)
			},
			createClan: function() {
				socket.emit('createClan', this.newClan)
			},
			removeClan: function() {
				socket.emit('removeClan')
			},
			leaveClan: function() {
				socket.emit('leaveClan')
			},
			joinClan: function(clanId) {
				socket.emit('joinClan', clanId)
			},
			acceptRequest: function(userId) {
				socket.emit('acceptClanRequest', userId)
			},
			rejectRequest: function(userId) {
				socket.emit('rejectClanRequest', userId)
			},
			kickUser: function(userId) {
				socket.emit('kickUserFromClan', userId)
			}
		}
	})

	return () => {}
}