module.exports = (Vue, socket, storage) => {

	let inventoryEl = new Vue({
		el: '.inventoryMenu',
		data: storage,
		computed: {
			statValues: function() {
				let statValues = []
				for(let statKey in storage.statTranslations) {
					statValues.push({
						value: storage.methods.countOwnStatsForKey(statKey),
						name: storage.statTranslations[statKey],
						key: statKey
					})
				}
				return statValues
			},
			inventory: function() {
				return Object.values(storage.items)
					.filter(item => item.owner === storage.me && !item.deleted)
					.sort((a, b) => new Date(a.timeGot) > new Date(b.timeGot) ? -1 : 1)
					.sort((a, b) => {
						if(b.enabled && !a.enabled)
							return 1
						if(a.enabled && !b.enabled)
							return -1
						else
							return 0
					})
			}
		}
	})

	let shopEl = new Vue({
		el: '.shopMenu',
		data: storage
	})

	return () => {}
}