import alertify from 'alertifyjs'

module.exports = (Vue, socket, storage) => {

	new Vue({
		el: '.notificationMenu',
		data: storage,
		methods: {
			clearNotifications: function() {
				storage.notifications.splice(0, storage.notifications.length)
			}
		}
	})

	for(let i = 1; i <= 5; i++) {
		new Vue({
			el: `.soundEmitter${i}`,
			data: storage,
			mounted: function() {
				this.$nextTick(function() {
					storage.audios.push(this.$refs.audio)
				})
			}
		})
	}

	socket.on('notification', (type, text, subType) => {
		if(!alertify.hasOwnProperty(type) || typeof alertify[type] !== 'function')
			type = 'message'

		storage.notifications.unshift({
			id: storage.notifications.length,
			type,
			text,
			date: new Date()
		})



		switch (type) {
			case 'success':
				if(subType === 'roll')
					storage.methods.playSound(`assets/audio/notificationRollSuccess.ogg`)
				else
					storage.methods.playSound(`assets/audio/notificationSuccess.ogg`)
				break;
			case 'error':
				storage.methods.playSound(`assets/audio/notificationError.ogg`)
				break;
			case 'message':
				storage.methods.playSound(`assets/audio/notificationMessage.ogg`)
				break;
			default:
				storage.methods.playSound(`assets/audio/notificationMessage.ogg`)
				break;
		}

		alertify[type](text)
	});

	return () => {}
}