//require("@babel/polyfill");

import './assets/index.css'
import './assets/index.less'
//require('../node_modules/vuejs-noty/dist/vuejs-noty.css')


import io from 'socket.io-client'
import Vue from 'vue'


import VueKonva from 'vue-konva'
Vue.use(VueKonva)

//

//import VueAlertify from 'vue-alertify';
//Vue.use(VueAlertify)
/*import VueNoty from 'vuejs-noty'
Vue.use(VueNoty, {
	timeout: 4000,
	progressBar: true,
	layout: 'bottomLeft',
	theme: 'metroui'
})*/

let socket = io()

/*
const ClientJS = require('clientjs');
console.log(ClientJS)
const client = new ClientJS()
const fingerprint = client.getFingerprint();*/
const statTranslations = {
	luck: 'Удача',
	defence: 'Защита',
	/*efficiency: 'Эффективность',
	capacity: 'Ёмкость',
	power_saving: 'Энергосбережение',
	another_stat: '%none%'*/
}
const ranks = require('./ranks.json')

function mixColors(c1, c2, sp) {
	const maxConst = 100 / Math.log2(ranks[ranks.length - 1].score);

	let weight = (Math.log2(sp + 1) * maxConst)

	let color = "#";

	for(let i = 0; i <= 5; i += 2) {
		let v1 = parseInt(c1.substr(i, 2), 16),
			v2 = parseInt(c2.substr(i, 2), 16),
			val = (Math.floor(v2 + (v1 - v2) * (weight / 100))).toString(16)

		color += val.padStart(2, '0');
	}

	return color;
};

for(let rank of ranks) {
	rank.color = mixColors('B400FF', '00FF00', rank.score);
}

const storage = {
	authUser: {
		login: '',
		password: '',
		shouldCreate: false
	},
	editUser: null,
	editClan: null,
	newClan: {
		name: '',
		color: '#333333'
	},
	searchClan: '',
	foundClans: [],
	clans: {},
	notifications: [],
	menu: {
		user: false,
		settings: false,
		clan: false,
		inventory: false,
		shop: false,
		notifications: false,
		show: function(menu) {
			storage.menu.hideMenus()
			storage.menu[menu] = true
		},
		hideMenus: function() {
			for(let otherMenu in storage.menu) {
				if(typeof storage.menu[otherMenu] !== 'function')
					storage.menu[otherMenu] = false
			}
		}
	},
	tooltip: {
		user: null,
		land: null,
		item: null,
		clan: null,
		size: {
			h: 0,
			w: 0
		},
		position: {
			x: 0,
			y: 0
		}
	},
	online: [],
	isOnline: false,
	me: null,
	users: {},
	items: {},
	baseItems: {},
	selectedItem: null,
	shop: [],
	shopSignature: null,
	ranks,
	statTranslations,
	map: null,
	animatedPaths: {},
	rollButtonDisabled: false,
	giveLandUserId: null,
	chat: {
		message: '',
		messages: []
	},
	audios: [],
	methods: {
		updateDependentData: result => {
			storage.methods.updateClans(result.clans)
			storage.methods.updateBaseItems(result.baseItems)
			storage.methods.updateItems(result.items)
			storage.methods.updateUsers(result.users)

			if(result.shopSignature)
				storage.shopSignature = result.shopSignature

			if(typeof result.messages === 'object')
				storage.chat.messages = result.messages

			if(typeof result.shop === 'object')
				storage.shop = result.shop
		},
		updateUsers: users => {
			if(!(Symbol.iterator in Object(users)))
				return;

			for(let user of users) {
				user.rank = ranks.findIndex(el => el.score > user.totalsp) - 1

				Vue.set(storage.users, user.id, user)
			}
		},
		updateClans: clans => {
			if(!(Symbol.iterator in Object(clans)))
				return;

			for(let clan of clans) {
				if(clan.owner === storage.me)
					storage.editClan = Object.assign({}, clan)
				Vue.set(storage.clans, clan.id, clan)
			}
		},
		updateItems: items => {
			if(!(Symbol.iterator in Object(items)))
				return;

			for(let item of items) {
				Vue.set(storage.items, item.id, item)
			}
		},
		updateBaseItems: baseItems => {
			if(!(Symbol.iterator in Object(baseItems)))
				return;

			for(let baseItem of baseItems) {
				Vue.set(storage.baseItems, baseItem.id, baseItem)
			}
		},
		hideMenus: function() {
			for(let otherMenu in storage.menu) {
				if(typeof storage.menu[otherMenu] !== 'function')
					storage.menu[otherMenu] = false
			}
		},
		playSound: function(url) {
			let audio = storage.audios.find(audio => audio.paused)
			if(!audio)
				return setTimeout(storage.methods.playSound, 100, url)

			audio.volume = storage.settings.volume / 100
			audio.src = url
		},
		countOwnStatsForKey: function(key) {
			let baseItems = Object.values(storage.items)
				.filter(item => item.deleted === false)
				.filter(item => item.owner === storage.me)
				.filter(item => item.enabled === true)
				.map(item => storage.baseItems[item.baseItem])

			return storage.funcs.countItemsStat(baseItems, key)
		}
	},
	settings: {
		lightTheme: 0,
		easierMapBackground: 0,
		mapMode: 0,
		mapModes: 5,
		barSide: 0,
		brightness: 100,
		volume: 100,
		mapColor: {
			friendly: '#00aa00',
			enemy: '#aa0000'
		}
	},
	regexes: require('../helpers/regexes.js'),
	permissions: require('../helpers/permissions.js'),
	funcs: require('../helpers/funcs.js')
}

Vue.component('interactive-user', {
	props: ['user'],
	template: `
	<span>
		<span v-if="user === undefined">nouser</span>
		<span v-if="user !== undefined" class="user" :class="{['user-color-' + user.id % 5]: true, 'user-moderator': this.moderator, 'user-admin': this.admin}" @mouseenter="mouseEnter" @mouseleave="mouseLeave" @mousemove="mouseMove" @click="onClick">
			<span>{{user.name}}{{user.emoji}}</span>
		</span>
	</span>`,
	computed: {
		moderator: function() {
			return storage.permissions.moderators.includes(this.user.id)
		},
		admin: function() {
			return storage.permissions.admins.includes(this.user.id)
		}
	},
	methods: {
		onClick: function(e) {
			storage.chat.message = `@${this.user.id}, `
			document.querySelector('.chatInput').focus()
		},
		mouseEnter: function(e) {
			storage.tooltip.user = this.user.id
		},
		mouseMove: function(e) {
			storage.tooltip.position = {
				x: e.clientX,
				y: e.clientY
			}
		},
		mouseLeave: function(e) {
			storage.tooltip.user = null
		}
	}
})

Vue.component('interactive-land', {
	props: ['code'],
	template: `
	<span>
		<span v-if="land === undefined">{{code}}</span>
		<span class="interactiveLand" v-if="land !== undefined" @mouseenter="mouseEnter" @mouseleave="mouseLeave" @mousemove="mouseMove" @click="onClick">
			<span>{{land.name}} {{getEmojiByCode(code)}}</span>
		</span>
	</span>`,
	computed: {
		land: function() {
			return storage.map.paths[this.code]
		}
	},
	methods: {
		getEmojiByCode: function(code) {
			const chars = [...code.slice(0, 2)].map(c => c.charCodeAt() + 127397);
			return String.fromCodePoint(...chars);
		},
		onClick: function(e) {
			storage.methods.zoomToLand(this.code)
		},
		mouseEnter: function(e) {
			storage.tooltip.land = this.code
		},
		mouseMove: function(e) {
			storage.tooltip.position = {
				x: e.clientX,
				y: e.clientY
			}
		},
		mouseLeave: function(e) {
			storage.tooltip.land = null
		}
	}
})

Vue.component('interactive-clan', {
	props: ['clan'],
	template: `<span :class="{['clan-color-' + clan.id % 5]: true}" @mouseenter="mouseEnter" @mouseleave="mouseLeave" @mousemove="mouseMove">
		<span>{{clan.name}}</span>
	</span>`,
	methods: {
		mouseEnter: function(e) {
			storage.tooltip.clan = this.clan
		},
		mouseMove: function(e) {
			storage.tooltip.position = {
				x: e.clientX,
				y: e.clientY
			}
		},
		mouseLeave: function(e) {
			storage.tooltip.clan = null
		}
	}
})


Vue.component('interactive-join-clan', {
	props: ['clan'],
	template: `
	<span class="chatClan">
		<interactive-clan v-bind:clan="clan"></interactive-clan>
		<button v-if="!isAlreadyInClan" class="small" @click="join" :disabled="isDisabled">Вступить</button>
	</span>`,
	computed: {
		isDisabled: function() {
			return this.clan.requests.includes(storage.me)
		},
		isAlreadyInClan: function() {
			return storage.me && (this.clan.owner === storage.me || storage.users[storage.me].clan === this.clan.id)
		}
	},
	methods: {
		join: function() {
			socket.emit('joinClan', this.clan.id)
		}
	}
})


Vue.component('interactive-item', {
	props: ['item', 'baseItem', 'onlyIcon'],
	template: `<span @mouseenter="mouseEnter" @mouseleave="mouseLeave" @mousemove="mouseMove">
		<img :src="'/assets/sprites/Item_' + baseItem.id + '.png'" class="itemIcon" />
		<span v-if="!onlyIcon" :class="{['item-rarity-' + baseItem.rarity]: true}">{{baseItem.name}}</span>
	</span>`,
	methods: {
		mouseEnter: function(e) {
			storage.tooltip.item = this.item.id
			storage.tooltip.baseItem = this.baseItem.id
		},
		mouseMove: function(e) {
			storage.tooltip.position = {
				x: e.clientX,
				y: e.clientY
			}
		},
		mouseLeave: function(e) {
			storage.tooltip.item = null
			storage.tooltip.baseItem = null
		}
	}
})

Vue.component('interactive-inventory-item', {
	props: ['item', 'baseItem'],
	template: `
	<div class="inventoryItem">
		<div @click="select" @dblclick="toggle" :style="{filter: item.enabled ? '' : 'grayscale(1)'}">
			<interactive-item v-bind:item="item" v-bind:base-item="baseItem" v-bind:onlyIcon="true"></interactive-item>
		</div>
	</div>`,
	methods: {
		countItemPrice: storage.funcs.countItemPrice,
		select: function() {
			storage.selectedItem = this.item.id
		},
		toggle: function() {
			socket.emit('toggleItem', this.item.id)
		}
	}
})

Vue.component('interactive-selected-item', {
	props: ['item', 'baseItem'],
	template: `
	<div class="inventoryItem">
		<div @click="select" :style="{filter: item.enabled ? '' : 'grayscale(1)'}">
			<interactive-item v-bind:item="item" v-bind:base-item="baseItem"></interactive-item>
		</div>
		<div>
			<button class="small" @click="sell">{{countItemPrice(baseItem.rarity, baseItem.price, item.uses, baseItem.maxUses)}} <span class="iconify" data-icon="fa:gg"></span></button>
			<button class="small" @click="toggle" :class="{ok: item.enabled, error: !item.enabled}">{{item.enabled ? 'on' : 'off'}}</button>
			<button class="small" @click="share">
				<span class="iconify" data-icon="entypo:chat"></span>
			</button>
		</div>
	</div>`,
	methods: {
		countItemPrice: storage.funcs.countItemPrice,
		select: function() {
			storage.selectedItem = this.item.id
		},
		toggle: function() {
			socket.emit('toggleItem', this.item.id)
		},
		share: function() {
			socket.emit('chatWrite', `$${this.item.id}`)
		},
		sell: function() {
			socket.emit('sellItem', this.item.id)
		}
	}
})

Vue.component('interactive-shop-item', {
	props: ['baseItem'],
	template: `
	<span>
		<interactive-item v-bind:item="{}" v-bind:base-item="baseItem"></interactive-item>
		<button @click="buy">Купить за {{countItemPrice(baseItem.rarity, baseItem.price, baseItem.maxUses, baseItem.maxUses, true)}} <span class="iconify" data-icon="fa:gg"></span></button>
	</span>`,
	methods: {
		countItemPrice: storage.funcs.countItemPrice,
		buy: function() {
			socket.emit('buyItem', this.baseItem.id)
		}
	}
})

Vue.component('interactive-date', {
	props: ['date', 'onlyTime'],
	template: `<span :title="date.toLocaleString()">{{onlyTime ? '' : date.toLocaleDateString() + ' в '}}{{date.toLocaleTimeString()}}</span>`
})

Vue.component('interactive-message', {
	props: ['message'],
	render: function(createElement) {
		let messageParts = []

		if(storage.permissions.moderators.includes(storage.me) || this.message.from === storage.me)
			messageParts.push(createElement('span', {
				on: {
					click: e => {
						socket.emit('removeMessage', this.message.id)
					}
				},
				class: {
					removeMessage: true
				}
			}, 'x'))

		messageParts.push(createElement('interactive-date', {
			props: {
				date: new Date(this.message.date),
				onlyTime: true
			},
			class: {
				chatTime: true
			}
		}))

		messageParts.push(createElement('interactive-user', {
			props: {
				user: storage.users[this.message.from]
			}
		}))

		if(this.message.private)
			messageParts.push(' > '),
			messageParts.push(createElement('interactive-user', {
				props: {
					user: storage.users[this.message.to]
				}
			}))

		messageParts.push(': ')

		let interactives = 0

		let interactiveText = this.message.text.split(storage.regexes.mentionGroup)
			.flatMap(el => el.split(storage.regexes.itemMentionGroup))
			.flatMap(el => el.split(storage.regexes.clanMentionGroup))
			.flatMap(el => el.split(storage.regexes.landMentionGroup))
			.map(el => {
				if(interactives > 5)
					return el
				else if(storage.regexes.mentionGroup.test(el)) {
					let user = storage.users[el.slice(1)]
					if(!user)
						return el

					interactives++;

					return createElement(
						'interactive-user', {
							props: {
								user
							}
						}
					)
				} else if(storage.regexes.itemMentionGroup.test(el)) {
					let item = storage.items[el.slice(1)]
					if(!item)
						return el

					interactives++;

					let baseItem = storage.baseItems[item.baseItem]

					return createElement(
						'interactive-item', {
							props: {
								item,
								baseItem
							}
						}
					)
				} else if(storage.regexes.clanMentionGroup.test(el)) {
					let clan = storage.clans[el.slice(1)]
					if(!clan)
						return el

					interactives++;

					return createElement(
						'interactive-join-clan', {
							props: {
								clan
							}
						}
					)
				} else if(storage.regexes.landMentionGroup.test(el)) {
					let code = el.slice(1).toUpperCase()
					let land = storage.map.paths[code]
					if(!land)
						return el

					interactives++;

					return createElement(
						'interactive-land', {
							props: {
								code
							}
						}
					)
				} else
					return el
			})

		messageParts.push(createElement('span', {}, interactiveText))

		return createElement('span', {
			class: {
				admin: storage.permissions.admins.includes(storage.me),
					moderator: storage.permissions.moderators.includes(storage.me),
					guest: storage.permissions.guests.includes(storage.me)
			}
		}, messageParts)
	}
})

async function init() {
	let users = await require('./users.js')(Vue, socket, storage)
	let settings = await require('./settings.js')(Vue, socket, storage)
	let items = await require('./items.js')(Vue, socket, storage)
	let clans = await require('./clans.js')(Vue, socket, storage)
	let map = await require('./map.js')(Vue, socket, storage)
	let chat = await require('./chat.js')(Vue, socket, storage)
	let notification = await require('./notification.js')(Vue, socket, storage)
	let tooltip = await require('./tooltip.js')(Vue, socket, storage)

	socket.on('connect', e => {
		storage.isOnline = true
		users()
		settings()
		items()
		clans()
		map()
		chat()
		notification()
		tooltip()
	})

	socket.on('disconnect', e => {
		storage.isOnline = false
	})

	new Vue({
		el: '.opaqueFiller',
		data: storage
	})
}
init()