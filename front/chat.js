module.exports = (Vue, socket, storage) => {
	new Vue({
		el: '.chatControls',
		data: storage,
		methods: {
			sendMessage: function() {
				if(this.chat.message.trim() === '')
					return false;

				socket.emit('chatWrite', this.chat.message)
				this.chat.message = ''
			}
		}
	})
	new Vue({
		el: '.chatMessages',
		data: storage,
	})

	socket.on('chatRead', result => {
		let messages = result.messages

		delete result.messages
		storage.methods.updateDependentData(result)

		storage.chat.messages.unshift(...messages)
	});

	socket.on('removeMessage', messageId => {
		let messageIndex = storage.chat.messages.findIndex(message => message.id === messageId)
		if(messageIndex === -1)
			return

		storage.chat.messages.splice(messageIndex, 1)
	})

	return () => {
		storage.chat.messages = []
	}
}