const paths = require('./mapSource.min.json').paths
import * as d3 from './d3.js'
import App from './App.vue';

module.exports = (Vue, socket, storage) => {
	for(let code in paths) {
		let path = paths[code]
		path.owner = null
		path.power = null
		path.animated = false
	}

	storage.map = {
		paths: paths,
		target: null
	}

	new Vue({
		el: '#app',
		components: {
			App
		},
		template: '<App/>'
	});

	const pathsEl = new Vue({
		el: '.mapElement',
		data: storage,
		computed: {
			patterns: function() {
				if(this.settings.mapMode !== 0)
					return []

				return Object.entries(this.map.paths).reduce((acc, [code, land]) => {
					if(acc.find(fill => fill.owner === land.owner))
						return acc
					if(land.owner === null)
						return acc

					let user = this.users[land.owner]
					if(!user)
						return acc

					let userClan = this.clans[user.clan]
					if(!userClan)
						return acc

					if(user.color === userClan.color)
						return acc

					acc.push({
						owner: land.owner,
						id: `pattern${land.owner}`,
						color1: user.color,
						color2: userClan.color
					})

					return acc
				}, [])
			},
			fills: function() {
				return Object.entries(this.map.paths).reduce((acc, [code, land]) => {
					if(land.owner === null)
						return acc

					let user = this.users[land.owner]
					if(!user)
						return acc

					let userClan = this.clans[user.clan]

					let fill = user.color
					let groupValue = land.owner

					if(userClan && this.settings.mapMode === 0 && userClan.color !== user.color)
						fill = `url(#pattern${land.owner})`
					else if(userClan && this.settings.mapMode === 1)
						fill = userClan.color,
						groupValue = user.clan
					else if(this.settings.mapMode === 3) {
						let thisUser = this.users[this.me]
						if(thisUser) {
							if(thisUser.id === land.owner || (thisUser.clan && thisUser.clan === user.clan))
								fill = this.settings.mapColor.friendly,
								groupValue = 1
							else
								fill = this.settings.mapColor.enemy,
								groupValue = 0
						}
					} else if(this.settings.mapMode === 4) {
						fill = this.ranks[user.rank].color
						groupValue = user.rank
					}

					let pathRecord = {
						code,
						animated: land.animated,
						path: land.path
					}

					let existingRecord = acc.find(fill => fill.groupValue === groupValue)
					if(existingRecord) {
						existingRecord.paths.push(pathRecord)
					} else {
						acc.push({
							owner: land.owner,
							groupValue,
							paths: [pathRecord],
							fill
						})
					}

					return acc;
				}, [])
			},
			animatedPathsList: function() {
				return new Set(Object.keys(this.animatedPaths))
			}
		},
		methods: {
			selectTarget: function(e) {
				const code = e.target.getAttribute('code')
				this.map.target = code
				storage.methods.playSound('assets/audio/landHit.ogg')
			},
			mouseEnter: function(e) {
				this.tooltip.land = e.target.getAttribute('code')
			},
			mouseMove: function(e) {
				this.tooltip.position = {
					x: e.clientX,
					y: e.clientY
				}
			},
			mouseLeave: function(e) {
				this.tooltip.land = null
			},
			animatePaths: function(codes) {
				let timeoutId = Math.random()
				for(let code of codes) {
					Vue.set(this.animatedPaths, code, timeoutId)
				}
				setTimeout(e => {
					Object.entries(this.animatedPaths).forEach(([code, timeout]) => {
						if(timeout === timeoutId)
							Vue.delete(this.animatedPaths, code)
					})
				}, 1000)
			}
		}
	})

	new Vue({
		el: '.giveLand',
		data: storage,
		computed: {
			isDisabled: function() {
				let target = this.map.paths[this.map.target]

				return this.giveLandUserId === null || !target || target.owner !== this.me
			}
		},
		methods: {
			giveLand: function() {
				socket.emit('giveLand', this.map.target, this.giveLandUserId)
				console.log()
			}
		}
	})

	const rollButton = new Vue({
		el: '.rollButton',
		data: storage,
		computed: {
			rollButtonText: function() {
				if(!this.map.target)
					return 'Выберите территорию'

				let target = this.map.paths[this.map.target]
				let sameClan = this.users[this.me].clan && this.users[target.owner].clan === this.users[this.me].clan

				let text = ''

				if(target.owner === this.me || sameClan)
					if(target.power === 3)
						return 'Максимальное улучшение'
				else
					text = 'Улучшить'
				else
					text = 'Захватить'

				return text + ` "${target.name}"`
			},
			isDisabled: function() {
				if(this.rollButtonDisabled || !this.map.target)
					return true

				let target = this.map.paths[this.map.target]
				return target.owner === this.me && target.power === 3
			}
		},
		methods: {
			roll: function(e) {
				if(!e.isTrusted)
					return;

				socket.emit('roll', this.map.target);
				storage.rollButtonDisabled = true
				storage.methods.playSound('assets/audio/buttonFired.ogg')
				setTimeout(e => {
					storage.rollButtonDisabled = false
					storage.methods.playSound('assets/audio/buttonReady.ogg')
				}, 1000)
			}
		}
	})


	let zoom = {
		mapElement: d3.select('.mapElement'),
		svg: d3.select('.mapElement svg'),
		group: d3.select('.mapElement svg g'),
		zoom: d3.zoom()
			.scaleExtent([1, 100])
			.translateExtent([
				[0, 0],
				[900, 500]
			])
			.on("zoom", function() {
				let e = d3.event;

				zoom.group.attr('style', `transform: translate(${e.transform.x}px, ${e.transform.y}px)scale(${e.transform.k})`)

				if(!e.sourceEvent)
					return

				if(e.sourceEvent.type === 'touchmove')
					pathsEl.mouseLeave()
				else
					pathsEl.mouseMove(e.sourceEvent)
			}),
		init: function() {
			this.mapElement
				.call(this.zoom)
				.on('dblclick.zoom', null)

			zoom.zoom.scaleTo(zoom.group, 1)
		}
	}

	storage.methods.zoomToLand = function(code) {
		let bbox = document.querySelector(`[code="${code}"]`).getBBox()
		let windowBox = zoom.mapElement.node().getBoundingClientRect()
		zoom.zoom.scaleTo(zoom.mapElement, Math.min(windowBox.height / bbox.height, windowBox.width / bbox.width))
		zoom.zoom.translateTo(zoom.mapElement, bbox.x + bbox.width / 2, bbox.y + bbox.height / 2)
	}

	socket.on('updateMap', result => {
		storage.methods.updateDependentData(result)
		pathsEl.animatePaths(result.lands.map(land => land.code))
		for(let land of result.lands) {
			storage.map.paths[land.code].owner = land.owner
			storage.map.paths[land.code].power = land.power
		}
	});

	socket.once('updateMap', e => {
		zoom.init()
	})

	return () => {}
}