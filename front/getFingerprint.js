import Fingerprint2 from 'fingerprintjs2'
import * as crypto from 'browserify-aes'

module.exports = {
	get: seed => {
		return new Promise(resolve => {
			Fingerprint2.get(components => {
				const pairValue = components.map(pair => {
					return [pair.key, pair.value].join()
				}).join()
				let cipher = crypto.createCipheriv('aes-256-ctr', 'woro'.repeat(8), seed.slice(0, 16))
				let encoded = cipher.update(Fingerprint2.x64hash128(pairValue, 31))
				resolve(encoded)
			})
		})
	}
}