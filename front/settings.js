import Verte from 'verte';
import 'verte/dist/verte.css';

module.exports = (Vue, socket, storage) => {
	let menuEl = new Vue({
		el: '.settingsMenu',
		data: storage,
		components: {
			Verte
		},
		computed: {
			contrastingColorEnemy: function() {
				return (this.settings.mapColor.enemy.replace('#', '0x')) > (0xffffff / 2) ? '#000' : '#fff';
			},
			contrastingColorFriendly: function() {
				return (this.settings.mapColor.friendly.replace('#', '0x')) > (0xffffff / 2) ? '#000' : '#fff';
			},
			mapModeName: function() {
				switch (this.settings.mapMode) {
					case 0:
						return 'Кланы и лидеры'
					case 1:
						return 'Кланы'
					case 2:
						return 'Лидеры'
					case 3:
						return 'Тактический'
					case 4:
						return 'Ранги'
					default:
						return this.settings.mapMode
				}
			}
		},
		mounted() {
			if(localStorage.lightTheme)
				this.settings.lightTheme = parseInt(localStorage.lightTheme)

			if(localStorage.easierMapBackground)
				this.settings.easierMapBackground = parseInt(localStorage.easierMapBackground)

			if(localStorage.mapMode)
				this.settings.mapMode = parseInt(localStorage.mapMode)

			if(localStorage.mapColorFriendly)
				this.settings.mapColor.friendly = localStorage.mapColorFriendly

			if(localStorage.mapColorEnemy)
				this.settings.mapColor.enemy = localStorage.mapColorEnemy

			if(localStorage.barSide)
				this.settings.barSide = parseInt(localStorage.barSide)

			if(localStorage.brightness)
				this.settings.brightness = parseInt(localStorage.brightness)

			if(localStorage.volume)
				this.settings.volume = parseInt(localStorage.volume)
		},
		watch: {
			'settings.lightTheme': function(newValue) {
				localStorage.lightTheme = newValue
				this.calculateBodyClasslist()
			},
			'settings.easierMapBackground': function(newValue) {
				localStorage.easierMapBackground = newValue
			},
			'settings.mapMode': function(newValue) {
				localStorage.mapMode = newValue
			},
			'settings.mapColor.friendly': function(newValue) {
				localStorage.mapColorFriendly = newValue
			},
			'settings.mapColor.enemy': function(newValue) {
				localStorage.mapColorEnemy = newValue
			},
			'settings.barSide': function(newValue) {
				localStorage.barSide = newValue
				this.calculateBodyClasslist()
			},
			'settings.brightness': function(newValue) {
				localStorage.brightness = newValue
			},
			'settings.volume': function(newValue) {
				localStorage.volume = newValue
			}
		},
		methods: {
			toggleTheme: function(e) {
				this.settings.lightTheme = +!this.settings.lightTheme
			},
			toggleMapBackground: function(e) {
				this.settings.easierMapBackground = +!this.settings.easierMapBackground
			},
			changeMapMode: function(e) {
				this.settings.mapMode = (this.settings.mapMode + 1) % this.settings.mapModes
			},
			toggleBarSide: function(e) {
				this.settings.barSide = +!this.settings.barSide
			},
			calculateBodyClasslist: function() {
				document.body.classList = []
				document.body.classList.add(this.settings.lightTheme === 1 ? 'lightTheme' : 'darkTheme')
				document.body.classList.add(this.settings.barSide === 1 ? 'rightBar' : 'leftBar')
			}
		}
	})

	return () => {}
}