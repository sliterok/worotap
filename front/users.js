import Verte from 'verte';
import 'verte/dist/verte.css';

const countGlyphs = require('stringz').length

module.exports = async (Vue, socket, storage) => {
	const fingerprint = require('./getFingerprint.js')

	let authEl = new Vue({
		el: '.auth',
		data: storage,
		methods: {
			auth: async function() {
				socket.emit('sendAuth', this.authUser, await fingerprint.get(socket.id))
			}
		},
		computed: {
			loginOk: function() {
				return storage.regexes.login.test(this.authUser.login)
			},
			passwordOk: function() {
				return storage.regexes.password.test(this.authUser.password)
			}
		}
	})

	let onlineEl = new Vue({
		el: '.online',
		data: storage
	})

	let menuEl = new Vue({
		el: '.userMenu',
		data: storage,
		components: {
			Verte
		},
		computed: {
			contrastingColor: function() {
				return (this.editUser.color.replace('#', '0x')) > (0xffffff / 2) ? '#000' : '#fff';
			},
			isEditNameValid: function() {
				return this.regexes.name.test(this.editUser.name)
			},
			isEditColorValid: function() {
				return this.regexes.color.test(this.editUser.color)
			},
			isEditEmojiValid: function() {
				return countGlyphs(this.editUser.emoji) <= this.editUser.emojis
			}
		},
		methods: {
			saveValue: function(key) {
				let val = this.editUser[key]
				let valid = this.regexes[key] ? this.regexes[key].test(val) : true
				if(!valid)
					return console.error(`tried to save ${key} but it's value of "${val}" is invalidated on client-side`)

				socket.emit('editUser', key, val)
			},
			logout: function() {
				localStorage.removeItem('session')
				socket.emit('getAuth')
			}
		}
	})

	let menuButtonsEl = new Vue({
		el: '.menuButtons',
		data: storage
	})

	socket.on('setUser', userId => {
		if(storage.permissions.admins.includes(userId))
			window.storage = storage,
			window.socket = socket

		storage.me = userId
		let user = storage.users[userId]
		storage.editUser = Object.assign({}, user)
		if(user.clan)
			storage.editClan = Object.assign({}, storage.clans[user.clan])
	})

	socket.on('setSession', async session => {
		localStorage.setItem('session', session)
		socket.emit('getAuth', session, await fingerprint.get(socket.id))
	})

	socket.on('updateOnline', result => {
		storage.methods.updateDependentData(result)

		if(result.online) {
			storage.online = result.online
		} else if(result.changeOnline) {
			let index = storage.online.findIndex(onlineRecord => onlineRecord.user === result.changeOnline.user)
			if(index !== -1)
				storage.online.splice(index, 1, result.changeOnline)
			else
				storage.online.push(result.changeOnline)
		} else if(result.removeOnline) {
			let index = storage.online.findIndex(onlineRecord => onlineRecord.user === result.removeOnline)
			if(index !== -1)
				storage.online.splice(index, 1)
		}
	});

	return async () => {
		socket.emit('getAuth', localStorage.getItem('session'), await fingerprint.get(socket.id))
	}
}