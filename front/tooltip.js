module.exports = (Vue, socket, storage) => {
	let tooltipEl = new Vue({
		el: '.tooltip',
		data: storage,
		watch: {
			'tooltip.user': function(newValue) {
				Vue.nextTick(e => this.recalculateSize())
			},
			'tooltip.land': function(newValue) {
				Vue.nextTick(e => this.recalculateSize())
			},
			'tooltip.item': function(newValue) {
				Vue.nextTick(e => this.recalculateSize())
			},
			'tooltip.clan': function(newValue) {
				Vue.nextTick(e => this.recalculateSize())
			},
			'tooltip.position.x': function(newValue) {
				if(newValue + this.tooltip.size.w > window.innerWidth - 20)
					this.tooltip.position.x = window.innerWidth - this.tooltip.size.w - 20
			},
			'tooltip.position.y': function(newValue) {
				if(newValue + this.tooltip.size.h > window.innerHeight - 20)
					this.tooltip.position.y = window.innerHeight - this.tooltip.size.h - 20
			}
		},
		methods: {
			recalculateSize: function() {
				let el = document.querySelector('.tooltip')
				if(!el)
					return;
				let styles = window.getComputedStyle(el, null)
				storage.tooltip.size = {
					h: parseFloat(styles.getPropertyValue("height")),
					w: parseFloat(styles.getPropertyValue("width"))
				}
			}
		}
	})

	return () => {}
}