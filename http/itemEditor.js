const Table = require('../helpers/spreadSheetApi.js')
const mongoose = require('../mongo.js')
const BaseItem = mongoose.models.BaseItem

module.exports = app => {
	app.get('/edititems', async (req, res) => {
		return res.sendFile('edititems.html', {
			root: __dirname
		})
	})

	app.get('/listitems', async (req, res) => {
		return res.send(await Table.getTable())
	})

	app.post('/updateitem', async (req, res) => {
		let itemObj = req.body;
		let itemArr = [];

		paramList = ['id', 'name_ru', 'name_en', 'rarity', 'cost', 'price', 'drops', 'stats', 'uses', 'description_ru', 'laser_color']

		paramList.forEach(param => {
			itemArr.push(itemObj[param] === undefined ? null : itemObj[param])
		})

		console.log(itemArr);

		let rs = await Table.updateItem(itemObj.id, itemArr);
		res.send('ok')
	})

	app.post('/syncitemswithdb', async (req, res) => {
		const spreadsheet = (await Table.getTable())
			.filter(el => el.drops === 'TRUE' || el.drops === 'FALSE')
			.map(el => {
				return {
					insertOne: {
						document: {
							id: parseInt(el.id),
							name: el.name_ru,
							drops: el.drops === 'TRUE',
							stats: el.stats === '' ? null : el.stats.split('&').reduce((acc, stat) => {
								let splitStat = stat.split('=')
								acc[splitStat[0]] = parseFloat(splitStat[1])
								return acc
							}, {}),
							maxUses: parseInt(el.uses),
							rarity: parseInt(el.rarity),
							cost: parseInt(el.cost),
							price: el.price === '' ? undefined : parseInt(el.price),
							description: el.description_ru === '' ? undefined : el.description_ru
						}
					}
				}
			})

		for(let item of spreadsheet.filter(item => item.price)) {
			console.log(item)
		}

		let destroy = await BaseItem.deleteMany({})

		await BaseItem.bulkWrite(spreadsheet);

		res.send('kk bro')
	})
}