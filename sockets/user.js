const secrets = require('../secrets.js')
const crypto = require('crypto')
const mongoose = require('../mongo.js')
const User = mongoose.models.User
const Clan = mongoose.models.Clan
const Message = mongoose.models.Message
const Item = mongoose.models.Item
const MobileDetect = require('mobile-detect')

const countGlyphs = require('stringz').length

const regexes = require('../helpers/regexes.js')
const permissions = require('../helpers/permissions.js')

const fieldsAllowedToEdit = ['name', 'color', 'emoji']

async function clientDisconnected(socket, storage) {
	let deviceTypeBefore = storage.methods.getOnlineRecordDeviceType(storage.online[socket.user.id])

	let userFullyDisconnected = false
	if(storage.online.hasOwnProperty(socket.user.id) && storage.online[socket.user.id].length > 1) {
		let index = storage.online[socket.user.id].findIndex(subRecord => subRecord.socket === socket.id)
		if(index !== -1)
			storage.online[socket.user.id].splice(index, 1)
	} else if(storage.online.hasOwnProperty(socket.user.id)) {
		delete storage.online[socket.user.id]
		userFullyDisconnected = true
	}

	socket.leave(`u${socket.user.id}`)
	if(socket.user.clan)
		socket.leave(`c${socket.user.clan}`)

	storage.shouldUpdateUsers.delete(socket.user.id)

	let deviceTypeAfter = storage.methods.getOnlineRecordDeviceType(storage.online[socket.user.id])

	socket.user.online += Math.floor(Date.now() / 1000) - socket.onlineCounter
	await socket.user.save()

	if(userFullyDisconnected)
		socket.broadcast.emit('updateOnline', {
			removeOnline: socket.user.id.toString()
		})
	else if(!userFullyDisconnected && deviceTypeBefore !== deviceTypeAfter)
		socket.broadcast.emit('updateOnline', {
			changeOnline: {
				user: socket.user.id.toString(),
				device: deviceTypeAfter
			},
			users: [user.filterFields()],
			clans: await storage.methods.requireClans([user])
		})
}

module.exports = async function(io, socket, storage) {
	socket.on('disconnect', async function() {
		if(socket.user)
			await clientDisconnected(socket, storage)
	})

	socket.on('getAuth', async (session, key) => {
		let decipher = crypto.createDecipheriv('aes-256-ctr', 'woro'.repeat(8), socket.id.slice(0, 16))
		if(key)
			socket.key = decipher.update(key, 'Buffer', 'utf8')

		let user = await User.findOne({
			'auths.session': session,
			'auths.key': socket.key
		})

		if(!user)
			user = await User.findOne({
				id: 10
			})

		if(socket.user)
			await clientDisconnected(socket, storage)

		socket.user = user
		socket.onlineCounter = Math.floor(Date.now() / 1000)
		socket.join(`u${socket.user.id}`)
		if(socket.user.clan)
			socket.join(`c${socket.user.clan}`)


		let md = new MobileDetect(socket.handshake.headers['user-agent'])

		let subRecord = {
			device: md.mobile() !== null ? 'mobile' : 'desktop',
			socket: socket.id
		}

		let deviceTypeBefore = storage.methods.getOnlineRecordDeviceType(storage.online[user.id])

		if(!storage.online.hasOwnProperty(user.id))
			storage.online[user.id] = [subRecord]
		else
			storage.online[user.id].push(subRecord)

		let deviceTypeAfter = storage.methods.getOnlineRecordDeviceType(storage.online[user.id])

		if(deviceTypeBefore !== deviceTypeAfter)
			socket.broadcast.emit('updateOnline', {
				changeOnline: {
					user: user.id.toString(),
					device: deviceTypeAfter
				},
				users: [user.filterFields()],
				clans: await storage.methods.requireClans([user])
			})


		let userQuery = {
			$or: [{
				id: storage.methods.getOnlineList()
			}]
		}

		if(user.clan) {
			let userClan = await Clan.findOne({
				id: user.clan
			})
			if(userClan.owner === user.id)
				userQuery.$or.push({
					id: userClan.requests
				})


			userQuery.$or.push({
				clan: user.requests
			})
		}

		let itemQuery = {
			$or: [{
				owner: user.id,
				deleted: false
			}]
		}

		let filters = {
			$or: [{
				private: false,
			}, {
				private: true,
				$or: [{
					from: socket.user.id
				}, {
					to: socket.user.id
				}]
			}]
		}

		if(permissions.admins.includes(user.id))
			filters = {}

		let messages = await Message.find(filters, null, {
			sort: {
				id: -1,
			},
			limit: 500
		})

		userQuery.$or.push({
			id: messages.flatMap(message => message.users)
		})

		itemQuery.$or.push({
			id: messages.flatMap(message => message.items)
		})

		let requiredUsers = await User.findFilteredQuery(userQuery)
		let requiredClans = await storage.methods.requireClans(requiredUsers)

		let requiredItems = await Item.findFilteredQuery(itemQuery)
		let requiredBaseItems = await storage.methods.requireBaseItems(requiredItems)

		socket.emit('updateOnline', {
			online: storage.methods.getOnlineRecords(),
			users: requiredUsers,
			clans: requiredClans,
			items: requiredItems,
			baseItems: requiredBaseItems,
			messages: messages.map(message => message.filterFields()),
			shopSignature: crypto.createHash('md5').update(`worldroulette::0:4hbxm3TDV3Jgtveh:shp_uid=${user.id}`).digest("hex")
		})

		socket.emit('setUser', user.id)
	});

	socket.on('sendAuth', async someUser => {
		if(!regexes.login.test(someUser.login))
			return socket.emit('notification', 'error', `Логин не прошел проверку!`)
		if(!regexes.password.test(someUser.password))
			return socket.emit('notification', 'error', `Пароль не прошел проверку!`)

		let passHash = crypto.pbkdf2Sync(someUser.password, secrets.passHash, 1000, 64, 'sha512').toString(`hex`)
		let newSession = crypto.randomBytes(64).toString('hex');

		let existingUser = await User.findOne({
			login: someUser.login
		})

		console.log('sendAuth someUser:', someUser)
		if(someUser.shouldCreate) {
			//return socket.emit('notification', 'error', 'Регистрация на данный момент отключена!')
			if(existingUser)
				return socket.emit('notification', 'error', `Пользователь с таким логином уже зарегистрирован!`)

			user = await User.create({
				login: someUser.login,
				auths: [{
					session: newSession,
					key: socket.key
				}],
				password: passHash,
			})

			socket.emit('setSession', newSession)
		} else {
			if(!existingUser)
				return socket.emit('notification', 'error', `Пользователя с таким логином не существует!`)

			if(existingUser.password !== passHash) {
				const oldPassHash = crypto.createHmac('sha512', secrets.oldPassHash).update(someUser.password).digest('base64');
				if(existingUser.oldPass !== oldPassHash)
					return socket.emit('notification', 'error', `Неверный пароль!`)
			}

			existingUser.auths.push({
				session: newSession,
				key: socket.key
			})
			await existingUser.save()

			socket.emit('setSession', newSession)
		}
	});

	socket.on('editUser', async (key, value) => {
		if(socket.user.id === 10)
			return socket.emit('notification', 'error', 'Данная операция запрещена на гостевом аккаунте')
		if(!fieldsAllowedToEdit.includes(key))
			return socket.emit('notification', 'error', `Данное поле запрещено редактировать!`)
		if(socket.user[key] === value)
			return socket.emit('notification', 'message', `Вы ничего не изменили...`)

		if(key === 'emoji') {
			let emojiCount = countGlyphs(value)
			if(emojiCount > socket.user.emojis)
				return socket.emit('notification', 'error', `Вам доступно ${socket.user.emojis} эмодзи, но вы ввели ${emojiCount}!`)
		} else if(!regexes[key].test(value))
			return socket.emit('notification', 'error', `Поле не прошло проверку!`)

		socket.user[key] = value
		await socket.user.save()

		io.emit('updateOnline', {
			users: [socket.user.filterFields()]
		})
		socket.emit('notification', 'success', `Вы успешно изменили ${key} на ${value}`)
	})
}