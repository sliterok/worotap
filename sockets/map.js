const mongoose = require('../mongo.js')
const rwc = require('random-weighted-choice')

const {
	RateLimiterMemory
} = require('rate-limiter-flexible');

const rateLimiter = new RateLimiterMemory({
	points: 1,
	duration: 1
});

const Land = mongoose.models.Land
const User = mongoose.models.User
const Clan = mongoose.models.Clan
const Item = mongoose.models.Item
const BaseItem = mongoose.models.BaseItem

const initialise = require('../helpers/initialise.js')
const funcs = require('../helpers/funcs.js')
let lands = null

const maxPower = 3

module.exports = async function(io, socket, storage) {
	let init = await initialise.init('Land')
	if(init !== true)
		lands = init

	let neededUsers = await requireUsers(lands)
	let neededClans = await storage.methods.requireClans(neededUsers)

	socket.emit('updateMap', {
		users: neededUsers,
		clans: neededClans,
		lands: filterLands(lands)
	})


	socket.on('giveLand', async function(code, targetUserId) {
		if(parseInt(targetUserId) === socket.user.id)
			return socket.emit('notification', 'error', `Вы не можете передать территорию самому себе!`)

		let land = lands.find(land => land.code === code)
		if(!land)
			return socket.emit('notification', 'error', `Территории "${code}" не существует`)
		if(land.owner !== socket.user.id)
			return socket.emit('notification', 'error', `Территория "${code}" не принадлежит вам`)

		let newOwner = await User.findOne({
			id: targetUserId
		})
		if(!newOwner)
			return socket.emit('notification', 'error', `Человека с id: ${targetUserId} не существует`)

		let landPrice = 20
		if(socket.user.balance - landPrice < 0)
			return socket.emit('notification', 'error', `Вам не хватает ${landPrice - socket.user.balance} gg.!`)

		socket.user.balance -= landPrice
		await socket.user.save()

		land.owner = targetUserId
		await land.save()

		let neededUsers = await requireUsers([land])
		neededUsers.push(socket.user.filterFields())
		let neededClans = await storage.methods.requireClans(neededUsers)

		socket.emit('updateMap', {
			users: neededUsers,
			clans: neededClans,
			lands: filterLands([land])
		})

		io.to(`u${socket.user.id}`).emit('notification', 'success', `Вы передали "${code}" ${newOwner.name}!`)
	})


	socket.on('roll', async function(code) {
		let land = lands.find(land => land.code === code)
		if(!land)
			return socket.emit('notification', 'error', `Территории "${code}" не существует`)

		let owner = await User.findOne({
			id: land.owner
		})


		let type = 'attack'
		if(socket.user.id === owner.id)
			type = 'defence'
		else if(socket.user.clan && socket.user.clan === owner.clan)
			type = 'defence'

		if(type === 'defence' && land.power >= maxPower)
			return socket.emit('notification', 'error', `Территория уже максимально улучшена`)

		try {
			await rateLimiter.consume(socket.user.id)
			await rateLimiter.consume(socket.handshake.address)
		} catch (e) {
			return socket.emit('notification', 'error', `Слишком быстро!`)
		}


		let initiatorBundle = await getBundledItems(socket.user.id)
		let luck = initiatorBundle.getStat('luck')
		let initiatorUsedItems = await initiatorBundle.useStat('luck')

		io.to(`u${socket.user.id}`).emit('updateOnline', {
			items: initiatorUsedItems.map(item => item.filterFields())
		})

		if(type === 'attack') {
			let recipientBundle = await getBundledItems(owner.id)
			luck -= recipientBundle.getStat('defence')
			let recipientUsedItems = await recipientBundle.useStat('defence')

			io.to(`u${owner.id}`).emit('updateOnline', {
				items: recipientUsedItems.map(item => item.filterFields())
			})
		}

		if(type === 'attack') {
			let neighbors = land.neighbors.filter(neighborCode => {
				let land = lands.find(land => land.code === neighborCode)
				if(!land)
					return false;
				return land.owner === socket.user.id
			}).length
			luck += neighbors < 1 ? -1 : (neighbors - 1) * 0.1
		}

		let rollResult = doRoll(socket.user.rollsFromLast, luck)

		let stoppedCombos = [rollResult.comboName]

		if(socket.user.id !== 10 && shouldDrop(socket.user.rollsFromLast.drop)) {
			stoppedCombos.push('drop')

			let newItem = await rewardPlayer(socket.user)
			io.to(`u${socket.user.id}`).emit('updateOnline', {
				items: [newItem.filterFields()],
				baseItems: await storage.methods.requireBaseItems([newItem])
			})

			io.to(`u${socket.user.id}`).emit('notification', 'success', 'Вам выпал новый предмет!')
		}

		await updateRollsFromLast(socket.user, stoppedCombos)

		if(rollResult.power === 0)
			return socket.emit('notification', 'error', `Вам выпало ${rollResult.number}, попробуйте ещё раз!`)

		if(type === 'defence') {
			newLandPower = land.power + rollResult.power
		} else if(type === 'attack') {
			newLandPower = land.power - rollResult.power
		}

		if(newLandPower < 1)
			land.owner = socket.user.id,
			newLandPower = Math.abs(newLandPower) + 1

		if(newLandPower > maxPower)
			newLandPower = maxPower

		land.power = newLandPower

		await land.save()

		io.emit('updateMap', {
			lands: filterLands([land])
		})

		socket.emit('notification', 'success', `Вам выпало ${rollResult.number}!`, 'roll')

		if(type === 'attack')
			io.to(`u${owner.id}`).emit('notification', 'error', `${socket.user.name} атаковал вашу территорию!`)

		socket.user.totalsp += rollResult.power
		await socket.user.save()
	});
}

async function requireUsers(lands) {
	let userIds = new Set(lands.flatMap(land => land.owner))
	userIds.delete(null)

	return await User.findFiltered(Array.from(userIds))
}

async function getBundledItems(userId) {
	let items = await getItems(userId)
	let baseItems = await getBaseItems(items)

	return {
		items,
		baseItems,
		getStat: function(stat) {
			let value = this.filterByStat(stat).reduce((acc, item) => {
				let newValue = this.baseItems.find(baseItem => item.baseItem === baseItem.id).stats[stat]
				return acc + newValue
			}, 0)

			if(value > 1.5)
				return 1.5
			if(value < -1.5)
				return -1.5

			return value
		},
		useStat: async function(stat) {
			let filteredByStat = this.filterByStat(stat)
			for(let item of filteredByStat) {
				item.uses -= 1

				if(item.uses < 1)
					item.deleted = true;

				await item.save()
			}
			return filteredByStat
		},
		filterByStat: function(stat) {
			return this.items.filter(item => {
				let baseItem = this.baseItems.find(baseItem => item.baseItem === baseItem.id)
				if(!baseItem || baseItem.stats === null)
					return false
				return baseItem.stats.hasOwnProperty(stat)
			})
		}
	}
}

async function getItems(userId) {
	return await Item.find({
		enabled: true,
		deleted: false,
		owner: userId
	})
}

async function getBaseItems(items) {
	return await BaseItem.find({
		id: items.map(item => item.baseItem)
	})
}

async function rewardPlayer(user, rarity = parseInt(rwc(rolls.itemDistribution))) {
	let baseItem = (await BaseItem.aggregate()
		.match({
			rarity,
			drops: true
		})
		.sample(1)
		.exec())[0]

	if(rarity === -1)
		rarity += 2;

	if(!baseItem)
		return await rewardPlayer(user, --rarity);

	let newItem = await Item.create({
		baseItem: baseItem.id,
		owner: user.id,
		uses: baseItem.maxUses
	})

	return newItem
}

function shouldDrop(rollsFromLastDrop) {
	return rollHasCombination(rollsFromLastDrop, rolls.combinations.drop, 0)
}

async function updateRollsFromLast(user, stoppedCombos) {
	for(let comboName in rolls.combinations) {
		if(stoppedCombos.includes(comboName))
			user.rollsFromLast[comboName] = 0
		else
			user.rollsFromLast[comboName]++
	}
	return await user.save()
}

function doRoll(rollsFromLast, luck) {
	let number = getRandomDigit(9)
	let otherNumber = getRandomDigit(8)
	if(otherNumber === number)
		otherNumber = '9'
	let power = 0;
	let comboName = null
	for(let currComboName in rolls.combinations) {
		if(currComboName === 'drop')
			continue;

		let comboInfo = rolls.combinations[currComboName]
		let rollsSinceLast = rollsFromLast[currComboName]

		if(rollHasCombination(rollsSinceLast, comboInfo, luck)) {
			number = number.repeat(comboInfo.digits)
			power = comboInfo.power
			comboName = currComboName
			break;
		}
	}

	number = (otherNumber + number).padStart(4, Math.random().toString().slice(2))

	return {
		number,
		power,
		comboName
	}
}

function getRandomDigit(max) {
	return Math.floor(Math.random() * (max + 1)).toString()
}

function rollHasCombination(rollsSinceLast, combo, luck) {
	const maxLuck = combo.maxLuck
	const rollsConst = combo.const-(maxLuck * luck)
	const rollsCond = combo.condition - (maxLuck * luck)
	//console.log(rollsConst, rollsCond)

	return true //1 / (Math.exp(1 + (-rollsSinceLast * Math.random()) + (rollsConst * rollsSinceLast))) > rollsCond
}


function filterLands(lands) {
	return lands.map(land => {
		return {
			code: land.code,
			owner: land.owner,
			power: land.power
		}
	})
}

const rolls = {
	combinations: {
		drop: {
			condition: 0.383,
			const: 0.964,
			maxLuck: 0
		},
		double: {
			condition: 0.493,
			const: 0.612,
			maxLuck: 0.08,
			digits: 2,
			power: 1
		},
		triple: {
			condition: 0.50084,
			const: 0.99038,
			maxLuck: 0.008,
			digits: 3,
			power: 2
		},
		quadriple: {
			condition: 0.50314,
			const: 0.99997,
			maxLuck: 0.0008,
			digits: 4,
			power: 4
		}
	},
	itemDistribution: [{
			weight: 14,
			id: -1
		},
		{
			weight: 11,
			id: 0
		},
		{
			weight: 9,
			id: 1
		},
		{
			weight: 7,
			id: 2
		},
		{
			weight: 6,
			id: 3
		},
		{
			weight: 5,
			id: 4
		},
		{
			weight: 4,
			id: 5
		},
		{
			weight: 3,
			id: 6
		},
		{
			weight: 2,
			id: 7
		},
		{
			weight: 2,
			id: 8
		},
		{
			weight: 1,
			id: 9
		},
		{
			weight: 1,
			id: 10
		},
		{
			weight: 1,
			id: 11
		},
		{
			weight: 1,
			id: 12
		},
	]
}