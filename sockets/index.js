const mongoose = require('../mongo.js')
const {
	RateLimiterMemory
} = require('rate-limiter-flexible');

const User = mongoose.models.User
const Clan = mongoose.models.Clan
const Item = mongoose.models.Item
const BaseItem = mongoose.models.BaseItem

const shouldUpdateUsers = new Set()
const online = new Map()

const storage = {
	online: {},
	shouldUpdateUsers: new Set(),
	methods: {
		getOnlineList: function() {
			return Object.keys(storage.online)
		},
		getOnlineRecords: function() {
			return Object.entries(storage.online).map(onlineRecord => {
				return storage.methods.getOnlineRecord(...onlineRecord)
			})
		},
		getOnlineRecord: function(userId, subRecords) {
			return {
				user: userId,
				device: storage.methods.getOnlineRecordDeviceType(subRecords)
			}
		},
		getOnlineRecordDeviceType: function(subRecords) {
			if(!subRecords || subRecords.length < 1)
				return null

			return subRecords.find(subRecord => subRecord.device === 'desktop') ? 'desktop' : 'mobile'
		},
		getUserSockets: function(userId) {
			if(!storage.online.hasOwnProperty(userId))
				return []

			return storage.online[userId].map(subRecord => subRecord.socket)
		},
		requireUsers: async function(messages) {
			return await User.findFiltered(messages.flatMap(message => message.users))
		},
		requireClans: async function(users) {
			return await Clan.findFiltered(users.flatMap(user => user.clan))
		},
		requireItems: async function(input, includeDeleted) {
			let query = {}
			if(!includeDeleted)
				query.deleted = false

			if(input[0].items)
				query.id = input.flatMap(message => message.items)
			else
				query.owner = input.map(user => user.id)

			return await Item.findFilteredQuery(query)
		},
		requireBaseItems: async function(items) {
			return await BaseItem.findFiltered(items.flatMap(item => item.baseItem))
		},
		filterByOnline: async function(users) {
			return users.filter(user => online.has(user.id))
		}
	}
}

const rateLimiter = new RateLimiterMemory({
	points: 5,
	duration: 1,
	execEvenly: true
});

async function retryRequest(next, ip) {
	try {
		let consumed = await rateLimiter.consume(ip)
		next()
	} catch (rateLimitError) {
		setTimeout(retryRequest, rateLimitError.msBeforeNext, next, ip)
	}
}

module.exports = async function(io, socket) {
	socket.use(async (packet, next) => {
		let data = packet.length > 1 ? 'data: ' + JSON.stringify(packet.slice(1)) : 'no data'
		let userInfo = socket.user ? `user.id: ${socket.user.id}` : `anon user`
		console.log(`Socket event: ${packet[0]}, with ${data}, from ${userInfo}`)

		if(socket.user) {
			if(storage.shouldUpdateUsers.delete(socket.user.id))
				socket.user = await User.findOne({
					id: socket.user.id
				})
		} else if(packet[0] !== 'getAuth') {
			return next(new Error('Unauthorized error'))
		}

		retryRequest(next, socket.handshake.address)
	});
	await require('./user.js')(io, socket, storage)
	await require('./chat.js')(io, socket, storage)
	await require('./map.js')(io, socket, storage)
	await require('./clan.js')(io, socket, storage)
	await require('./item.js')(io, socket, storage)
}