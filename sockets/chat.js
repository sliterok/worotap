const mongoose = require('../mongo.js')
const Message = mongoose.models.Message
const User = mongoose.models.User
const Clan = mongoose.models.Clan
const Item = mongoose.models.Item

const regexes = require('../helpers/regexes.js')
const permissions = require('../helpers/permissions.js')

module.exports = async function(io, socket, storage) {
	socket.on('chatWrite', async function(text) {
		let users = new Set([socket.user.id])
		while(result = regexes.mention.exec(text)) {
			if(users.size > 5)
				break
			let userId = parseInt(result[1])
			let user = await User.findOne({
				id: userId
			})
			if(user)
				users.add(userId);
		}

		let items = new Set([])
		while(result = regexes.itemMention.exec(text)) {
			if(items.size > 5)
				break
			let itemId = parseInt(result[1])
			let item = await Item.findOne({
				id: itemId,
				owner: socket.user.id
			})
			if(item)
				items.add(itemId);
		}
		items = Array.from(items)

		let to = regexes.privateMessage.exec(text)
		if(to) {
			let toUser = await User.findOne({
				id: to[1]
			})

			if(!toUser) {
				to = null
				socket.emit('notification', 'error', `Такого пользователя не существует!`)
			} else {
				text = to[2]
				to = parseInt(to[1])
				users.add(to)
			}
		}
		users = Array.from(users)

		let clans = new Set([])
		while(result = regexes.clanMention.exec(text)) {
			if(clans.size > 5)
				break
			let clanId = parseInt(result[1])
			let clan = await Clan.findOne({
				id: clanId,
				owner: socket.user.id
			})
			if(clan)
				clans.add(clanId);
		}

		let message = await Message.create({
			text,
			users,
			items,
			clans,
			to,
			from: socket.user.id,
			private: to !== null
		})

		let requiredUsers = await storage.methods.requireUsers([message])
		for(let user of requiredUsers) {
			if(user.clan !== null)
				clans.add(user.clan)
		}
		clans = Array.from(clans)
		let requiredClans = await Clan.findFiltered(clans)
		let requiredItems = await storage.methods.requireItems([message], true)
		let requiredBaseItems = await storage.methods.requireBaseItems(requiredItems)

		let resultEmit = {
			users: requiredUsers,
			clans: requiredClans,
			items: requiredItems,
			baseItems: requiredBaseItems,
			messages: [message.filterFields()]
		}

		if(to === null) {
			socket.user.messages += 1
			await socket.user.save()

			io.emit('chatRead', resultEmit)

			for(let user of users) {
				if(user !== socket.user.id)
					io.to(`u${user}`).emit('notification', 'success', `${socket.user.name} упомянул вас в своем сообщении`)
			}
		} else {
			io.to(`u${to}`).to(`u${socket.user.id}`).emit('chatRead', resultEmit)

			for(let admin in permissions.admins) {
				io.to(`u${admin}`).emit('chatRead', resultEmit)
			}

			io.to(`u${to}`).emit('notification', 'success', `${socket.user.name} отправил вам приватное сообщение`)
		}
	});

	socket.on('removeMessage', async function(messageId) {

		let message = await Message.findOne({
			id: messageId
		})
		if(!message)
			return socket.emit('Такого сообщения не существует')

		if(!permissions.moderators.includes(socket.user.id) && message.from !== socket.user.id)
			return socket.emit('Вы не можете делать этого!')

		await message.remove()

		io.emit('removeMessage', messageId)
	})
}