const mongoose = require('../mongo.js')
const Item = mongoose.models.Item
const BaseItem = mongoose.models.BaseItem
const User = mongoose.models.User

const funcs = require('../helpers/funcs.js')

module.exports = async function(io, socket, storage) {
	let shopItems = await BaseItem.findFilteredQuery({
		drops: false
	})

	socket.emit('updateOnline', {
		baseItems: shopItems,
		shop: shopItems.map(item => item.id)
	})

	socket.on('buyItem', async baseItemId => {
		let baseItem = await BaseItem.findOne({
			id: baseItemId,
			drops: false
		})
		if(!baseItem)
			return socket.emit('notification', 'error', `Предмета не существует либо он не продается!`)

		let itemPrice = funcs.countItemPrice(baseItem.rarity, baseItem.price, baseItem.maxUses, baseItem.maxUses, true)
		if(socket.user.balance - itemPrice < 0)
			return socket.emit('notification', 'error', `У вас недостаточно средств для покупки данного предмета!`)

		socket.user.balance -= itemPrice
		await socket.user.save()

		let newItem = await Item.create({
			baseItem: baseItem.id,
			owner: socket.user.id,
			uses: baseItem.maxUses
		})

		io.emit('updateOnline', {
			users: [socket.user.filterFields()]
		})

		io.to(`u${socket.user.id}`).emit('updateOnline', {
			items: [newItem.filterFields()]
		})

		socket.emit('notification', 'success', `Вы купили предмет за ${itemPrice} gg.`)
	})

	socket.on('sellItem', async itemId => {
		let item = await Item.findOne({
			id: itemId,
			deleted: false
		})
		if(!item)
			return socket.emit('notification', 'error', `Предмета не существует`)

		let baseItem = await BaseItem.findOne({
			id: item.baseItem
		})

		item.deleted = true
		await item.save()

		let itemPrice = funcs.countItemPrice(baseItem.rarity, baseItem.price, item.uses, baseItem.maxUses)

		socket.user.balance += itemPrice
		await socket.user.save()

		io.emit('updateOnline', {
			users: [socket.user.filterFields()]
		})

		io.to(`u${socket.user.id}`).emit('updateOnline', {
			items: [item.filterFields()]
		})

		socket.emit('notification', 'success', `Вы продали предмет за ${itemPrice} gg.`)
	})

	socket.on('toggleItem', async itemId => {
		let item = await Item.findOne({
			id: itemId,
			deleted: false
		})
		if(!item)
			return socket.emit('notification', 'error', `Предмета не существует`)

		item.enabled = !item.enabled
		await item.save()

		io.to(`u${socket.user.id}`).emit('updateOnline', {
			items: [item.filterFields()]
		})
	})
}