const mongoose = require('../mongo.js')
const Clan = mongoose.models.Clan
const User = mongoose.models.User

const regexes = require('../helpers/regexes.js')

const fieldsAllowedToEdit = ['name', 'color']

module.exports = async function(io, socket, storage) {
	let fillingClans = await Clan.find().limit(5)
	socket.emit('foundClans', fillingClans.map(clan => clan.filterFields()))

	socket.on('createClan', async newClan => {
		if(socket.user.clan !== null)
			return socket.emit('notification', 'error', `Вы уже состоите в клане!`)

		if(!regexes.name.test(newClan.name))
			return socket.emit('notification', 'error', `Название не прошло проверку!`)
		if(!regexes.color.test(newClan.color))
			return socket.emit('notification', 'error', `Цвет не прошел проверку!`)

		let clan = await Clan.create({
			name: newClan.name,
			color: newClan.color,
			owner: socket.user.id
		})

		socket.user.clan = clan.id
		await socket.user.save()

		io.emit('updateOnline', {
			users: [socket.user.filterFields()],
			clans: [clan.filterFields()]
		})
		io.mergeRooms(`u${socket.user.id}`, `c${socket.user.clan}`)
		socket.emit('notification', 'success', `Вы успешно создали клан "${newClan.name}"`)
	})

	socket.on('removeClan', async () => {
		let clan = await Clan.findOne({
			owner: socket.user.id
		})
		if(!clan)
			return socket.emit('notification', 'error', `Вы не владелец клана!`)

		let clanUsers = await User.find({
			clan: socket.user.clan
		})

		let edits = []

		let filteredEdits = []

		for(let user of clanUsers) {
			user.clan = null
			let savePromise = user.save()
			savePromise.then(user => {
				if(storage.online.hasOwnProperty(user.id)) {
					storage.shouldUpdateUsers.add(user.id)
					filteredEdits.push(user.filterFields())
				}
			})
			edits.push(savePromise)
		}

		await Promise.all(edits);
		io.deleteRoom(`c${socket.user.clan}`)
		await clan.remove()

		io.emit('updateOnline', {
			users: filteredEdits
		})
		socket.emit('notification', 'success', `Вы успешно удалили клан "${clan.name}"`)
	})

	socket.on('acceptClanRequest', async userId => {
		let clan = await Clan.findOne({
			owner: socket.user.id
		})
		if(!clan)
			return socket.emit('notification', 'error', `Вы не владелец клана!`)

		let requestIndex = clan.requests.indexOf(userId)
		if(requestIndex === -1)
			return socket.emit('notification', 'error', `Этот пользователь не запрашивал вступление в Ваш клан!`)

		let requestUser = await User.findOne({
			id: userId
		})
		if(requestUser.clan)
			return socket.emit('notification', 'error', `Данный пользователь уже состоит в клане!`)

		clan.requests.splice(requestIndex, 1)
		await clan.save()

		requestUser.clan = clan.id
		await requestUser.save()

		if(storage.online.hasOwnProperty(userId)) {
			storage.shouldUpdateUsers.add(userId)
			io.emit('updateOnline', {
				users: [requestUser.filterFields()]
			})
			io.mergeRooms(`u${userId}`, `c${clan.id}`)
			io.to(`u${userId}`).emit('notification', 'success', `Вы были приняты в клан "${clan.name}"`)
		}

		io.to(`u${socket.user.id}`).emit('updateOnline', {
			clans: [clan.filterFields()]
		})

		socket.emit('notification', 'success', `Вы успешно приняли ${requestUser.name} в Ваш клан`)
	})

	socket.on('rejectClanRequest', async userId => {
		let clan = await Clan.findOne({
			owner: socket.user.id
		})
		if(!clan)
			return socket.emit('notification', 'error', `Вы не владелец клана!`)

		let requestIndex = clan.requests.indexOf(userId)
		if(requestIndex === -1)
			return socket.emit('notification', 'error', `Этот пользователь не запрашивал вступление в Ваш клан!`)

		let requestUser = await User.findOne({
			id: userId
		})

		clan.requests.splice(requestIndex, 1)
		await clan.save()

		if(storage.online.hasOwnProperty(userId))
			io.to(`u${userId}`).emit('notification', 'error', `Ваш запрос на вступление в клан "${clan.name}" был отклонен`)

		io.to(`u${socket.user.id}`).emit('updateOnline', {
			clans: [clan.filterFields()]
		})

		socket.emit('notification', 'success', `Вы успешно отклонили запрос на вступление ${requestUser.name} в Ваш клан`)
	})

	socket.on('kickUserFromClan', async userId => {
		if(userId === socket.user.id)
			return socket.emit('notification', 'error', `Вы не можете исключить самого себя!`)

		let clan = await Clan.findOne({
			owner: socket.user.id
		})
		if(!clan)
			return socket.emit('notification', 'error', `Вы не владелец клана!`)

		let requestUser = await User.findOne({
			id: userId
		})
		if(requestUser.clan !== clan.id)
			return socket.emit('notification', 'error', `Данный пользователь не состоит в вашем клане!`)

		requestUser.clan = null
		await requestUser.save()

		if(storage.online.hasOwnProperty(userId)) {
			storage.shouldUpdateUsers.add(userId)
			io.emit('updateOnline', {
				users: [requestUser.filterFields()]
			})
			io.intersectRooms(`u${userId}`, `c${clan.id}`)
			io.to(`u${userId}`).emit('notification', 'success', `Вас исключили из клана "${clan.name}"`)
		}

		socket.emit('notification', 'success', `Вы успешно исключили ${requestUser.name} из Вашего клана`)
	})

	socket.on('leaveClan', async () => {
		if(!socket.user.clan)
			return socket.emit('notification', 'error', `Вы не состоите в клане!`)

		let clan = await Clan.findOne({
			id: socket.user.clan
		})
		if(clan.owner === socket.user.id)
			return socket.emit('notification', 'error', `Владелец клана не может покинуть его!`)

		socket.user.clan = null
		await socket.user.save()

		io.emit('updateOnline', {
			users: [socket.user.filterFields()]
		})
		io.intersectRooms(`u${socket.user.id}`, `c${clan.id}`)
		socket.emit('notification', 'success', `Вы успешно покинули клан "${clan.name}"`)
	})

	socket.on('searchClan', async query => {
		if(typeof query !== 'string')
			return

		let clansByName = await Clan.find({
			$text: {
				$search: query
			}
		}).limit(5)

		let clansById = []
		if(!isNaN(parseInt(query)))
			clansById = await Clan.find({
				id: query
			}).limit(5)

		let fillingClans = await Clan.find().limit(5)

		let clans = trim(clansByName.concat(clansById).concat(fillingClans)).slice(0, 5)

		socket.emit('foundClans', clans.map(clan => clan.filterFields()))
	})

	socket.on('joinClan', async clanId => {
		if(socket.user.clan)
			return socket.emit('notification', 'error', `Вы уже состоите в клане!`)

		let clan = await Clan.findOne({
			id: clanId
		})
		if(!clan)
			return socket.emit('notification', 'error', `Данный клан не существует!`)

		if(clan.requests.includes(socket.user.id))
			return socket.emit('notification', 'error', `Вы уже отправляли запрос на присоединение к этому клану!`)

		if(socket.user.clan === clanId)
			return socket.emit('notification', 'error', `Вы уже отправляли запрос на присоединение к этому клану!`)

		clan.requests.push(socket.user.id)
		await clan.save()

		io.to(`u${clan.owner}`).emit('updateOnline', {
			clans: [clan.filterFields()],
			users: await User.findFiltered(clan.requests)
		})

		io.to(`u${clan.owner}`).emit('notification', 'success', `${socket.user.name} отправил запрос на присоединение к вашему клану`)

		socket.emit('notification', 'success', `Запрос на присоединение к клану "${clan.name}" был отправлен`)
	})

	socket.on('editClan', async (key, value) => {
		if(!fieldsAllowedToEdit.includes(key))
			return socket.emit('notification', 'error', `Данное поле запрещено редактировать!`)
		if(!regexes[key].test(value))
			return socket.emit('notification', 'error', `Поле не прошло проверку!`)

		let clan = await Clan.findOne({
			owner: socket.user.id
		})
		if(!clan)
			return socket.emit('notification', 'error', `Вы не владелец клана!`)

		clan[key] = value
		await clan.save()

		io.to(`c${clan.id}`).emit('updateOnline', {
			clans: [clan.filterFields()]
		})
		socket.emit('notification', 'success', `Вы успешно изменили ${key} на ${value} для клана "${clan.name}"`)
	})
}

function trim(items) {
	const ids = [];
	return items.filter(item => ids.includes(item.id) ? false : ids.push(item.id));
}